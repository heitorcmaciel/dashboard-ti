<?php
require './banco.php';

if (isset($_POST['import'])) {
    $filename = $_FILES['file']['tmp_name'];

    if ($_FILES['file']['size'] > 0) {
        $file = fopen($filename, "r");
        
        fgetcsv($file, 10000, ";");

        while (($getData = fgetcsv($file, 10000, ";")) !== FALSE) {
            if (!empty($getData)) {
                $categoria = isset($getData[1]) ? $getData[1] : '';
                $modelo = isset($getData[2]) ? $getData[2] : '';
                $estado = isset($getData[3]) ? $getData[3] : '';
                $quantidade = isset($getData[4]) ? $getData[4] : '';
                $localizacao = isset($getData[4]) ? $getData[4] : '';
                $marca = isset($getData[5]) ? $getData[5] : '';
                $equip = isset($getData[6]) ? $getData[6] : '';
                $processador = isset($getData[7]) ? $getData[7] : '';
                $placa_mae = isset($getData[8]) ? $getData[8] : '';
                $memoria = isset($getData[9]) ? $getData[9] : '';
                $hd = isset($getData[10]) ? $getData[10] : '';
                $headset_entrada = isset($getData[11]) ? $getData[11] : '';
                $headset_microfone = isset($getData[12]) ? $getData[12] : '';

                $sql = "INSERT INTO equipamentos (categoria, modelo, estado, quantidade, localizacao, marca, equip, processador, placa_mae, memoria, hd, headset_entrada, headset_microfone)
                        VALUES ('$categoria', '$modelo', '$estado', '$quantidade', '$localizacao', '$marca', '$equip', '$processador', '$placa_mae', '$memoria', '$hd', '$headset_entrada', '$headset_microfone')";

                $result = mysqli_query($conn, $sql);

                if (!$result) {
                    echo "<script type=\"text/javascript\">
                            alert(\"Error: Unable to import data.\");
                          </script>";
                    fclose($file);
                    exit();
                }
            }
        }

        fclose($file);
        header("Location: ./../admin/pages/dashboard.php");
        exit();
    }
}
?>