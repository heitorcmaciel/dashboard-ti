<?php
include 'banco.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();
$user_id = $_SESSION['id'];
$username = $_SESSION['username'];

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $categoria = $_POST['categoria'];
    $estado = $_POST['estado'];
    $quantidade = $_POST['quantidade'];
    $local = $_POST['local'];
    $marca = $_POST['marca'];

    $equip = isset($_POST['equip']) ? $_POST['equip'] : null;
    $modelo = isset($_POST['modelo']) ? $_POST['modelo'] : null;
    $processador = isset($_POST['processador']) ? $_POST['processador'] : null;
    $placa_mae = isset($_POST['placa_mae']) ? $_POST['placa_mae'] : null;
    $memoria = isset($_POST['memoria']) ? $_POST['memoria'] : null;
    $hd = isset($_POST['hd']) ? $_POST['hd'] : null;
    $headset_entrada = isset($_POST['headset_entrada']) ? $_POST['headset_entrada'] : null;
    $headset_microfone = isset($_POST['headset_microfone']) ? $_POST['headset_microfone'] : null;

    $sql = "INSERT INTO equipamentos (categoria, modelo, estado, quantidade, localizacao, marca, equip, processador, placa_mae, memoria, hd, headset_entrada, headset_microfone)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    $stmt = $conn->prepare($sql);
    if (!$stmt) {
        error_log("Erro preparando o banco: " . $conn->error);
        die("Erro preparando o banco: " . $conn->error);
    }

    $stmt->bind_param("sssssssssssss", $categoria, $modelo, $estado, $quantidade, $local, $marca, $equip, $processador, $placa_mae, $memoria, $hd, $headset_entrada, $headset_microfone);

    if ($stmt->execute()) {
        $item_id = $stmt->insert_id;
        $action = "Inserted new item with ID $item_id";
        $log_sql = "INSERT INTO logs (user_id, username, action, item_id) VALUES (?, ?, ?, ?)";
        $log_stmt = $conn->prepare($log_sql);
        $log_stmt->bind_param("issi", $user_id, $username, $action, $item_id);
        $log_stmt->execute();

        echo "Novo item registrado com sucesso.";
    } else {
        echo "Erro: " . $stmt->error;
    }
} else {
    echo "Método de requisição invalido.";
}
?>