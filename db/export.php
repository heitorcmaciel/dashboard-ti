<?php
require './banco.php';

echo "\xEF\xBB\xBF";

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=equipment.csv');

$output = fopen('php://output', 'w');

$headers = array('categoria', 'modelo', 'estado', 'quantidade', 'localizacao', 'marca', 'equip', 'processador', 'placa_mae', 'memoria', 'hd', 'headset_entrada', 'headset_microfone');
fputcsv($output, $headers, ';');

$sql = "SELECT categoria, modelo, estado, quantidade, localizacao, marca, equip, processador, placa_mae, memoria, hd, headset_entrada, headset_microfone FROM equipamentos";
$result = mysqli_query($conn, $sql);

if ($result) {
    while ($row = mysqli_fetch_assoc($result)) {
        fputcsv($output, $row, ';');
    }
} else {
    echo "Error executing query: " . mysqli_error($conn);
}

fclose($output);
?>