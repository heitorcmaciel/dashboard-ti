<?php
include 'banco.php';

$id = $_POST['id'];
$moveQuantity = $_POST['moveQuantity'];
$moveToLocal = strtoupper($_POST['moveToLocal']);

$sql = "SELECT * FROM equipamentos WHERE id = $id";
$result = $conn->query($sql);
$item = $result->fetch_assoc();

if ($item) {
    if ($moveQuantity > 0 && $moveQuantity <= $item['quantidade']) {
        $newQuantity = $item['quantidade'] - $moveQuantity;
        if ($newQuantity > 0) {
            $sqlUpdate = "UPDATE equipamentos SET quantidade = $newQuantity WHERE id = $id";
            $conn->query($sqlUpdate);
        } else {
            $sqlDelete = "DELETE FROM equipamentos WHERE id = $id";
            $conn->query($sqlDelete);
        }

        $conditions = ["localizacao = '$moveToLocal'"];
        foreach ($item as $key => $value) {
            if ($key != 'id' && $key != 'quantidade' && $key != 'localizacao' && !is_null($value) && $value !== '') {
                $conditions[] = "$key = '$value'";
            }
        }
        $sqlCheck = "SELECT id, quantidade FROM equipamentos WHERE " . implode(' AND ', $conditions);
        $resultCheck = $conn->query($sqlCheck);
        $targetItem = $resultCheck->fetch_assoc();

        if ($targetItem) {
            $newTargetQuantity = $targetItem['quantidade'] + $moveQuantity;
            $sqlUpdateTarget = "UPDATE equipamentos SET quantidade = $newTargetQuantity WHERE id = {$targetItem['id']}";
            $conn->query($sqlUpdateTarget);
        } else {
            $sqlInsert = "INSERT INTO equipamentos (categoria, quantidade, estado, localizacao, marca, processador, placa_mae, memoria, hd, headset_entrada, headset_microfone, modelo, equip) 
            VALUES ('{$item['categoria']}', $moveQuantity, '{$item['estado']}', '$moveToLocal', '{$item['marca']}', '{$item['processador']}', '{$item['placa_mae']}', '{$item['memoria']}', '{$item['hd']}', '{$item['headset_entrada']}', '{$item['headset_microfone']}', '{$item['modelo']}', '{$item['equip']}')";
            $conn->query($sqlInsert);
        }

        echo "Movimentação realizada com sucesso!";
        header('Location: ./../admin/pages/dashboard.php');
    } else {
        echo "Quantidade inválida!";
    }
} else {
    echo "Item não encontrado!";
}

$conn->close();
?>