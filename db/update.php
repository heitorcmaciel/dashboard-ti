<?php
include 'banco.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();
$user_id = $_SESSION['id'];
$username = $_SESSION['username'];

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST['id'];
    $categoria = $_POST['categoria'];
    $estado = $_POST['estado'];
    $quantidade = $_POST['quantidade'];
    $local = $_POST['local'];
    $marca = $_POST['marca'];

    $equip = isset($_POST['equip']) ? $_POST['equip'] : null;
    $modelo = isset($_POST['modelo']) ? $_POST['modelo'] : null;
    $processador = isset($_POST['processador']) ? $_POST['processador'] : null;
    $placa_mae = isset($_POST['placa_mae']) ? $_POST['placa_mae'] : null;
    $memoria = isset($_POST['memoria']) ? $_POST['memoria'] : null;
    $hd = isset($_POST['hd']) ? $_POST['hd'] : null;
    $headset_entrada = isset($_POST['headset_entrada']) ? $_POST['headset_entrada'] : null;
    $headset_microfone = isset($_POST['headset_microfone']) ? $_POST['headset_microfone'] : null;
   
    $sql = "UPDATE equipamentos 
            SET categoria = '$categoria', modelo = '$modelo', estado = '$estado', quantidade = $quantidade, 
                localizacao = '$local', marca = '$marca', equip = '$equip', processador = '$processador', placa_mae = '$placa_mae', 
                memoria = '$memoria', hd = '$hd', headset_entrada = '$headset_entrada', headset_microfone = '$headset_microfone'
            WHERE id = $id";

    if($conn->query($sql) === true) {
        $action = "Updated item with ID $id";
        $log_sql = "INSERT INTO logs (user_id, username, action, item_id) VALUES (?, ?, ?, ?)";
        $log_stmt = $conn->prepare($log_sql);
        $log_stmt->bind_param("issi", $user_id, $username, $action, $id);
        $log_stmt->execute();

        echo "Item atualizado com sucesso.";
        header('Location: ./../admin/pages/dashboard.php');
    } else {
        echo "Erro: " .$sql ."<br>" .$conn->error;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Item Atualizado.</title>
    <link rel="stylesheet" href="./css/styles.css">
</head>
<body>
<a href="./../admin/pages/dashboard.php" id="updVoltar">Voltar para Dashboard</a>
</body>
</html>