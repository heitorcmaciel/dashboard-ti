<?php
include './banco.php';

$sql = "SELECT * FROM equipamentos";
$result = $conn->query($sql);

$equipamentos = array();

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $equipamentos[] = $row;
    }
}

echo json_encode($equipamentos);

$conn->close();
?>
