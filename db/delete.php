<?php
include 'banco.php';

session_start();
$user_id = $_SESSION['id'];
$username = $_SESSION['username'];

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST['id'];

    $sql = "DELETE FROM equipamentos WHERE id = $id";

    if ($conn->query($sql) === TRUE) {
        $action = "Deleted item with ID $id";
        $log_sql = "INSERT INTO logs (user_id, username, action, item_id) VALUES (?, ?, ?, ?)";
        $log_stmt = $conn->prepare($log_sql);
        $log_stmt->bind_param("issi", $user_id, $username, $action, $id);
        $log_stmt->execute();

        echo "Item excluído com sucesso";
    } else {
        echo "Erro: " . $sql . "<br>" . $conn->error;
    }
    $conn->close();
}
?>