<?php

session_start();
include "./../../db/banco.php";

if (!isset($_SESSION['username'])) {
    header("Location: ./../../index.php");
    exit();
}

if ($_SESSION['role'] !== 'admin') {
    header("Location: ./../../index.php?error=Unauthorized access");
    exit();
}

$limit = 10;
$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
$start = ($page - 1) * $limit;

$countResult = mysqli_query($conn, "SELECT COUNT(*) AS total FROM users");
$countRow = mysqli_fetch_assoc($countResult);
$total = $countRow['total'];
$pages = ceil($total / $limit);

$sql = "SELECT username, role FROM users LIMIT $start, $limit";
$result = mysqli_query($conn, $sql);

?>

<!DOCTYPE html>
<html>
<head>
    <title>Usuários cadastrados</title>
    <link rel="stylesheet" href="./../../css/styles.css">
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        table th,
        table td {
            border: 1px solid #ddd;
            padding: 12px;
            text-align: left;
        }

        table th {
            background-color: var(--primary-color);
            color: white;
        }

        table tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        table tr:hover {
            background-color: #ddd;
        }
        .pagination {
            display: flex;
            justify-content: center;
            margin-top: 20px;
        }
        .pagination a {
            margin: 0 5px;
            padding: 8px 16px;
            text-decoration: none;
            border: 1px solid #ddd;
            color: #333;
        }
        .pagination a.active {
            background-color: #4CAF50;
            color: white;
            border: 1px solid #4CAF50;
        }
        .pagination a:hover:not(.active) {
            background-color: #ddd;
        }

        #userRegBtn {
            float: right;
        }
    </style>
</head>
<body>
<header>
    <a href="./register.php" id="userRegBtn"><img src="./../../css/img/new.png"></a>
    <a href="./dashboard.php" id="backButton">Voltar</a>
    <h2>Usuários registrados</h2>
</header>


<table>
    <tr>
        <th>Usuário</th>
        <th>Tipo</th>
    </tr>
    <?php
    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_assoc($result)) {
            echo "<tr><td>" . $row["username"]. "</td><td>" . $row["role"]. "</td></tr>";
        }
    } else {
        echo "<tr><td colspan='2'>No users found</td></tr>";
    }
    ?>
</table>

<div class="pagination">
    <?php for ($i = 1; $i <= $pages; $i++): ?>
        <a href="?page=<?= $i; ?>" class="<?= ($page == $i) ? 'active' : ''; ?>"><?= $i; ?></a>
    <?php endfor; ?>
</div>

</body>
</html>

<?php
mysqli_close($conn);
?>