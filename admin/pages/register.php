<?php
    include "./../../db/banco.php";
    echo $id;
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrar novo usuário</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }
        .login-container {
            background-color: white;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        h2 {
            margin-bottom: 20px;
        }
        input[type="text"], input[type="password"], select {
            width: calc(100% - 20px);
            padding: 10px;
            margin: 10px 0;
            border: 1px solid #ccc;
            border-radius: 4px;
        }
        .error {
            background: #F2DEDE;
            color: #A94442;
            padding: 10px;
            width: 95%;
            border-radius: 5px;
            margin: 20px auto;
        }
        a {
            background-color: rgba(221, 0, 0, 0.699);
            color: #fff;
            border: none;
            padding: 12px 24px;
            cursor: pointer;
            border-radius: 8px 16px;
            text-decoration: none;
            text-align: center;
            display: inline-block;
            transition: background-color 0.3s;
            width: fit-content;
            margin: auto;
            float:left;
        }

        a:hover {
            background-color: rgb(139, 0, 0)
        }

        input[type="submit"] {
            background-color: #008000;
            color: #fff;
            border: none;
            padding: 12px 24px;
            cursor: pointer;
            border-radius: 8px 16px;
            text-decoration: none;
            text-align: center;
            display: inline-block;
            transition: background-color 0.3s;
            width: fit-content;
            margin: auto;
            float:right;
        }

        input[type="submit"]:hover {
            background-color: #016401
        }
        header {
        background-color: #1B365D;
        color: #fff;
        padding: 20px;
        text-align: center;
        }
    </style>
</head>
<body>
        <div class="login-container">
        <h1>Registrar</h1>
        <form action="./../../login/register.php" method="post">
        <?php if (isset($_GET['error'])) { ?>
     		<p class="error"><?php echo $_GET['error']; ?></p>
     	<?php } ?>
            <label for="username">Usuário:</label>
            <input type="text" id="username" name="username" placeholder="Username" required>
            <label for="password">Senha:</label>
            <input type="text" id="password" name="password" placeholder="Password" required>
            <label for ="role">Permissão</label>
            <select id="role" name="role" required>
                <option value="">Selecione...</option>
                <option value="user">Usuário</option>
                <option value="admin">Administrador</option>
            </select>
            <input type="submit" value="Salvar">
            <a href="./users.php" id="editVoltar">Cancelar</a>
        </form>
    </div>
</body>
</html>