<?php
session_start();
include './../../db/banco.php';

if (isset($_SESSION['id']) && isset($_SESSION['username'])) {
    if ($_SESSION['role'] !== 'admin') {
        header("Location: ./../../index.php?error=Unauthorized access");
        exit();
    }
    ?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Logs - Inventário de TI</title>
    <link rel="stylesheet" href="./../../css/styles.css">
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        table th,
        table td {
            border: 1px solid #ddd;
            padding: 12px;
            text-align: left;
        }

        table th {
            background-color: var(--primary-color);
            color: white;
        }

        table tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        table tr:hover {
            background-color: #ddd;
        }
    </style>
</head>

<body>
    <header>
        <a href="./dashboard.php" id="backButton">Voltar</a>
        <h1>Logs</h1>
    </header>
    <main>
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>User ID</th>
                    <th>Username</th>
                    <th>Action</th>
                    <th>Item ID</th>
                    <th>Timestamp</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sql = "SELECT * FROM logs ORDER BY timestamp DESC";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        echo "<tr>";
                        echo "<td>" . $row['id'] . "</td>";
                        echo "<td>" . $row['user_id'] . "</td>";
                        echo "<td>" . $row['username'] . "</td>";
                        echo "<td>" . $row['action'] . "</td>";
                        echo "<td>" . $row['item_id'] . "</td>";
                        echo "<td>" . $row['timestamp'] . "</td>";
                        echo "</tr>";
                    }
                } else {
                    echo "<tr><td colspan='6'>No logs found</td></tr>";
                }
                ?>
            </tbody>
        </table>
    </main>
</body>

</html>
<?php
} else {
    header('Location: ./../../index.php');
}
?>