<?php 
include './../../db/banco.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$id = $_GET['id'];
$sql = "SELECT * FROM equipamentos WHERE id = $id";
$result = $conn->query($sql);
$item = $result->fetch_assoc();
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Item - Inventário de TI</title>
    <link rel="stylesheet" href="./../../css/styles.css">
</head>
<body>
    <header>
        <a href="./dashboard.php" id="editVoltar">Voltar</a> 
        <h1>Editar Item - Inventário de TI</h1>
    </header>
    <main>
        <form id="editarForm" method="post" action="./../../db/update.php">
            <input type="hidden" name="id" value="<?php echo $item['id']; ?>">
               
            <label for="categoria">Categoria:</label>
            <select id="categoria" name="categoria" required>
                <option value="computador" <?php echo $item['categoria'] == 'computador' ? 'selected' : ''; ?>>Computador</option>
                <option value="monitor" <?php echo $item['categoria'] == 'monitor' ? 'selected' : ''; ?>>Monitor</option>
                <option value="headset" <?php echo $item['categoria'] == 'headset' ? 'selected' : ''; ?>>Headset</option>
                <option value="equipamentos" <?php echo $item['categoria'] == 'equipamentos' ? 'selected' : ''; ?>>Equipamentos</option>
            </select>
            
            <div id="categoria-especifica"></div>
            
            <label for="quantidade">Quantidade:</label>
            <input type="number" id="quantidade" name="quantidade" value="<?php echo $item['quantidade']; ?>" required>

            <label for="estado">Estado:</label>
            <select name="estado" id="estado" required>
                <option value="Funcionando" <?php echo $item['estado'] == 'Funcionando' ? 'selected' : ''; ?>>Funcionando</option>
                <option value="Defeito" <?php echo $item['estado'] == 'Defeito' ? 'selected' : ''; ?>>Defeito</option>
            </select>
            
            <label for="local">Localização:</label>
            <input type="text" id="local" name="local" value="<?php echo $item['localizacao']; ?>" required>

            <label for="marca">Marca:</label>
            <input type="text" id="marca" name="marca" value="<?php echo $item['marca']; ?>" required>
            
            <div class="button-container">
                <button type="button" class="excluir" onclick="confirmDelete(<?php echo $item['id']; ?>)">Excluir</button>
            </div>
            <div class="button-container">
                <button type="submit" class="btnCadastrar">Salvar</button>
            </div>
        </form>

        <!-- Move Items Form -->
        <form id="moveItemsForm" method="post" action="./../../db/move.php">
            <input type="hidden" name="id" value="<?php echo $item['id']; ?>">
            
            <label for="moveQuantity">Quantidade a Mover:</label>
            <input type="number" id="moveQuantity" name="moveQuantity" min="1" max="<?php echo $item['quantidade']; ?>" required>
            
            <label for="moveToLocal">Mover para Local:</label>
            <input oninput="this.value = this.value.toUpperCase()" type="text" id="moveToLocal" name="moveToLocal" required>
            
            <div class="button-container">
                <button type="submit" class="btnCadastrar">Mover</button>
            </div>
        </form>
    </main>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const categoria = document.getElementById('categoria');
            const categoriaEspecifica = document.getElementById('categoria-especifica');
            
            function showCategoriaEspecificaFields(category, item) {
                categoriaEspecifica.innerHTML = '';

                if (category === 'computador') {
                    categoriaEspecifica.innerHTML = `
                        <label for="processador">Processador:</label>
                        <input type="text" id="processador" name="processador" value="<?php echo $item['processador']; ?>" required>
                        
                        <label for="placa_mae">Placa Mãe:</label>
                        <input type="text" id="placa_mae" name="placa_mae" value="<?php echo $item['placa_mae']; ?>" required>
                        
                        <label for="memoria">Memória:</label>
                        <input type="text" id="memoria" name="memoria" value="<?php echo $item['memoria']; ?>" required>
                        
                        <label for="hd">HD:</label>
                        <input type="text" id="hd" name="hd" value="<?php echo $item['hd']; ?>" required>
                    `;
                } else if (category === 'headset') {
                    categoriaEspecifica.innerHTML = `
                        <label for="headset_entrada">Entrada:</label>
                        <select name="headset_entrada" id="headset_entrada" required>
                            <option value="RJ11" <?php echo $item['headset_entrada'] == 'RJ11' ? 'selected' : ''; ?>>RJ11</option>
                            <option value="P2" <?php echo $item['headset_entrada'] == 'P2' ? 'selected' : ''; ?>>P2</option>
                            <option value="USB" <?php echo $item['headset_entrada'] == 'USB' ? 'selected' : ''; ?>>USB</option>
                        </select>
                        <label for="headset_microfone">Microfone:</label>
                        <select name="headset_microfone" id="headset_microfone" required>
                            <option value="funciona" <?php echo $item['headset_microfone'] == 'funciona' ? 'selected' : ''; ?>>Funciona</option>
                            <option value="quebrado" <?php echo $item['headset_microfone'] == 'quebrado' ? 'selected' : ''; ?>>Não Funciona</option>
                        </select>
                    `;
                } else if (category === 'monitor') {
                    categoriaEspecifica.innerHTML = `
                        <label for="modelo">Modelo:</label>
                        <input type="text" id="modelo" name="modelo" value="<?php echo $item['modelo']; ?>">
                    `;
                } else if (category === 'equipamentos') {
                    categoriaEspecifica.innerHTML = `
                        <label for="equip">Equipamento:</label>
                        <input type="text" id="equip" name="equip" value="<?php echo $item['equip']; ?>">
                    `;
                }
            }

            categoria.addEventListener('change', function() {
                showCategoriaEspecificaFields(this.value, {});
            });

            showCategoriaEspecificaFields(categoria.value, <?php echo json_encode($item); ?>);
        });

        function confirmDelete(id) {
            if (confirm('Tem certeza que deseja excluir este item?')) {
                fetch('./../../db/delete.php', {
                    method: 'POST',
                    body: new URLSearchParams({ id: id })
                })
                .then(response => response.text())
                .then(data => {
                    alert(data);
                    window.location = './dashboard.php';
                })
                .catch(error => console.error('Erro:', error));
            }
        }
    </script>
</body>
</html>