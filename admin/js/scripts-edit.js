document.addEventListener('DOMContentLoaded', function() {
    const form = document.getElementById('editForm');

    function loadInventory() {
        window.location = 'https://192.168.1.175/gabriel/';
    }

    if (form) {
        form.addEventListener('submit', function(event) {
            event.preventDefault();
            
            const formData = new FormData(this);
            
            fetch('./../../db/update.php', {
                method: 'POST',
                body: formData
            })
            .then(response => response.text())
            .then(data => {
                alert(data);
                this.reset();
                loadInventory();
            })
            .catch(error => console.error('Erro:', error));
        });
    } else {
        console.error('Formulário de edição não encontrado.');
    }
});
