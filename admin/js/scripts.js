document.addEventListener('DOMContentLoaded', function () {
    const form = document.getElementById('cadastroForm');
    const dashboard = document.getElementById('dashboard');
    const categoria = document.getElementById('categoria');
    const filterForm = document.getElementById('filterForm');
    const toggleFilterButton = document.getElementById('toggleFilterButton');

    if (dashboard) {
        toggleFilterButton.addEventListener('click', function () {
            if (filterForm.style.display === 'none' || filterForm.style.display === '') {
                filterForm.style.display = 'block';
                toggleFilterButton.textContent = 'Esconder Filtros';
            } else {
                filterForm.style.display = 'none';
                toggleFilterButton.textContent = 'Mostrar Filtros';
            }
        });
    }

    if (dashboard) {
        loadInventory();
    }

    if (dashboard) {
        const refreshButton = document.getElementById('refreshButton');
        refreshButton.addEventListener('click', function () {
            loadInventory();
        });
    }

    function addItemToDashboard(items) {
        if (dashboard) {
            dashboard.innerHTML = '';

            const locations = {};

            items.forEach(item => {
                if (!locations[item.localizacao]) {
                    locations[item.localizacao] = [];
                }
                locations[item.localizacao].push(item);
            });

            for (const location in locations) {
                const locationDiv = document.createElement('div');
                locationDiv.classList.add('location');

                const locationHeader = document.createElement('h2');
                locationHeader.textContent = location;
                locationHeader.classList.add('location-header');
                locationHeader.addEventListener('click', function () {
                    const contentDiv = this.nextElementSibling;
                    contentDiv.style.display = contentDiv.style.display === 'none' ? 'block' : 'none';
                });

                const locationContent = document.createElement('div');
                locationContent.classList.add('location-content');
                locationContent.style.display = 'none';

                locations[location].forEach(item => {
                    const itemDiv = document.createElement('div');
                    itemDiv.classList.add('item');
                    if (item.categoria === 'headset') {
                        itemDiv.innerHTML = `
                        <div class="item-content">
                            <h3>${item.categoria.toUpperCase()}</h3>
                            <p>Entrada: ${item.headset_entrada}</p>
                            <p>Microfone: ${item.headset_microfone}</p>
                            <p>Quantidade: ${item.quantidade}</p>
                            <p>Estado: ${item.estado}</p>
                            <p>Marca: ${item.marca}</p>
                            <button class="editar" data-id="${item.id}"><img src="./../../css/img/edit.png"></button>
                        </div>
                                `;
                    } else if (item.categoria === 'equipamentos') {
                        itemDiv.innerHTML = `
                        <div class="item-content">
                            <h3>${item.equip.toUpperCase()}</h3>
                            <p>Quantidade: ${item.quantidade}</p>
                            <p>Estado: ${item.estado}</p>
                            <p>Marca: ${item.marca}</p>
                            <button class="editar" data-id="${item.id}"><img src="./../../css/img/edit.png"></button>
                            </div>
                            `;
                    } else if (item.categoria === 'monitor') {
                        itemDiv.innerHTML = `
                        <div class="item-content">
                            <h3>${item.categoria.toUpperCase()}</h3>
                            <p>Modelo: ${item.modelo}</p>
                            <p>Quantidade: ${item.quantidade}</p>
                            <p>Estado: ${item.estado}</p>
                            <p>Marca: ${item.marca}</p>
                            <button class="editar" data-id="${item.id}"><img src="./../../css/img/edit.png"></button>
                            </div>
                            `;
                    } else {
                        itemDiv.innerHTML = `
                        <div class="item-content">
                            <h3>${item.categoria.toUpperCase()}</h3>
                            <p>Quantidade: ${item.quantidade}</p>
                            <p>Estado: ${item.estado}</p>
                            <p>Marca: ${item.marca}</p>
                            <button class="editar" data-id="${item.id}"><img src="./../../css/img/edit.png"></button>
                            </div>    
                            `;
                    }
                    locationContent.appendChild(itemDiv);
                });

                locationDiv.appendChild(locationHeader);
                locationDiv.appendChild(locationContent);
                dashboard.appendChild(locationDiv);
            }
        }
    }

    function filterItems() {
        const filtroCategoria = document.getElementById('filtroCategoria').value.toLowerCase();
        const filtroEstado = document.getElementById('filtroEstado').value.toLowerCase();
        const filtroLocal = document.getElementById('filtroLocal').value.toLowerCase();

        fetch('./../../db/fetch.php')
            .then(response => response.json())
            .then(data => {
                let filteredItems = data.filter(item => {
                    return (filtroCategoria === '' || item.categoria.toLowerCase().includes(filtroCategoria)) &&
                        (filtroEstado === '' || item.estado.toLowerCase() === filtroEstado) && (filtroLocal === '' || item.localizacao.toLowerCase() === filtroLocal);
                });

                addItemToDashboard(filteredItems);
            })
            .catch(error => console.error('Erro ao filtrar itens:', error));
    }

    if (filterForm) {
        filterForm.addEventListener('submit', function (event) {
            event.preventDefault();
            filterItems();
        });
    } else {
        console.error('Formulário de filtro não encontrado.');
    }

    function loadInventory() {
        fetch('./../../db/fetch.php')
            .then(response => response.json())
            .then(data => {
                addItemToDashboard(data);
            })
            .catch(error => console.error('Erro ao carregar inventário:', error));
    }

    if (dashboard) {
        dashboard.addEventListener('click', function (event) {
            if (event.target.classList.contains('editar')) {
                const id = event.target.getAttribute('data-id');
                window.location = "./../pages/editar.php?id=" + id;
            }
        });
    }

    if (form) {
        console.log('Formulario inicializado.');
        form.addEventListener('submit', function (event) {
            event.preventDefault();

            const formData = new FormData();

            formData.append('categoria', document.getElementById('categoria').value);
            formData.append('estado', document.getElementById('estado').value);
            formData.append('quantidade', document.getElementById('quantidade').value);
            formData.append('local', document.getElementById('local').value);
            formData.append('marca', document.getElementById('marca').value);

            let modelo = document.getElementById('modelo');
            if (categoria.value !== 'headset' && modelo !== null) formData.append('modelo', modelo.value);

            let processador = document.getElementById('processador');
            if (categoria.value === 'computador' && processador !== null) formData.append('processador', processador.value);

            let placa_mae = document.getElementById('placa_mae');
            if (categoria.value === 'computador' && placa_mae !== null) formData.append('placa_mae', placa_mae.value);

            let memoria = document.getElementById('memoria');
            if (categoria.value === 'computador' && memoria !== null) formData.append('memoria', memoria.value);

            let hd = document.getElementById('hd');
            if (categoria.value === 'computador' && hd !== null) formData.append('hd', hd.value);

            let headset_entrada = document.getElementById('headset_entrada');
            if (categoria.value === 'headset' && headset_entrada !== null) formData.append('headset_entrada', headset_entrada.value);

            let headset_microfone = document.getElementById('headset_microfone');
            if (categoria.value === 'headset' && headset_microfone !== null) formData.append('headset_microfone', headset_microfone.value);

            let equip = document.getElementById('equip');
            if (categoria.value === 'equipamentos' && equip !== null) formData.append('equip', equip.value);

            for (let [key, value] of formData.entries()) {
                console.log(`${key}: ${value}`);
            }
            fetch('./../../db/insert.php', {
                method: 'POST',
                body: formData
            })
                .then(response => response.text())
                .then(data => {
                    alert(data);
                    console.log(data);
                    this.reset();
                    window.location = './../pages/dashboard.php';
                    loadInventory();
                })
                .catch(error => console.error('Erro:', error));
        });
    } else {
        console.error('Formulário de cadastro não encontrado.');
    }
});