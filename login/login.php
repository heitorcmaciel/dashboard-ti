<?php

session_start(); 
include "./../db/banco.php";

if (isset($_POST['username']) && isset($_POST['password'])) {

	function validate($data){
       $data = trim($data);
	   $data = stripslashes($data);
	   $data = htmlspecialchars($data);
	   return $data;
	}

	$username = validate($_POST['username']);
	$pass = validate($_POST['password']);

	if (empty($username)) {
		header("Location: ./../index.php?error=User Name is required");
	    exit();
	}else if(empty($pass)){
        header("Location: ./../index.php?error=Password is required");
	    exit();
	}else{
		$sql = "SELECT * FROM users WHERE password='$pass' AND username='$username'";

		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) === 1) {
			$row = mysqli_fetch_assoc($result);
            if ($row['username'] === $username && $row['password'] === $pass) {
            	$_SESSION['username'] = $row['username'];
            	$_SESSION['email'] = $row['email'];
            	$_SESSION['id'] = $row['id'];
				$_SESSION['role'] = $row['role'];
                
                if ($row['role'] === 'admin') {
                    header("Location: ./../admin/pages/dashboard.php");
                } else if ($row['role'] === 'user') {
                    header("Location: ./../user/pages/dashboard.php");
                } else {
                    header("Location: ./../index.php?error=Invalid role");
                }
		        exit();
            } else{
				header("Location: ./../index.php?error=Incorect User name or password");
		        exit();
			}
		}else{
			header("Location: ./../index.php?error=Incorect User name or password");
	        exit();
		}
	}
	
}else{
	header("Location: ./../index.php");
	exit();
}

?>