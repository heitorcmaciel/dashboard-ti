<?php
session_start();
if (isset($_SESSION['id']) && isset($_SESSION['username'])) {
  ?>
<!DOCTYPE html>
  <html lang="pt-BR">
  
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard - Inventário de TI</title>
    <link rel="stylesheet" href="./../../css/styles.css">
  </head>
  
  <body>
    <header>
      <a href="./../../login/logout.php" id="logoutBtn"><img src="./../../css/img/logout.png"></a>
      <button id="refreshButton"><img src="./../../css/img/refreshBtn.png"></button>
      <h1>Dashboard - Inventário de TI</h1>
    </header>
    <main>
      <button id="toggleFilterButton">Mostrar Filtros</button>
      <form id="filterForm" style="display: none;">
        <label for="filtroCategoria">Filtrar por Categoria:</label>
        <select id="filtroCategoria" name="filtroCategoria" placeholder="Selecione...">
          <option value="">Todos</option>
          <option value="computador">Computador</option>
          <option value="headset">Headset</option>
          <option value="monitor">Monitor</option>
          <option value="equipamentos">Equipamentos</option>
        </select>
  
        <label for="filtroEstado">Filtrar por Estado:</label>
        <select id="filtroEstado" name="filtroEstado">
          <option value="">Todos</option>
          <option value="funcionando">Funcionando</option>
          <option value="defeito">Defeito</option>
        </select>
        <label for="filtroLocal">Filtrar por Local:</label>
        <input type="text" id="filtroLocal" name="filtroLocal">
  
        <button type="submit" id="filterButton">Filtrar</button>
      </form>
      <div id="dashboard">
      </div>
    </main>
    <script src="./../js/scripts.js"></script>
  </body>
  
  </html>
  <?php
} else {
  header('Location: ./../../index.php');
}
?>

