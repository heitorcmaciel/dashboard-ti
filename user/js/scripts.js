try {
	document.addEventListener('DOMContentLoaded', function () {
		const form = document.getElementById('cadastroForm');
		const dashboard = document.getElementById('dashboard');
		const categoria = document.getElementById('categoria');
		const filterForm = document.getElementById('filterForm');
		const toggleFilterButton = document.getElementById('toggleFilterButton');

		if (dashboard) {
			toggleFilterButton.addEventListener('click', function () {
				if (filterForm.style.display === 'none' || filterForm.style.display === '') {
					filterForm.style.display = 'block';
					toggleFilterButton.textContent = 'Esconder Filtros';
				} else {
					filterForm.style.display = 'none';
					toggleFilterButton.textContent = 'Mostrar Filtros';
				};
			});
		};

		if (dashboard) {
			loadInventory();
		};

		if (dashboard) {
			const refreshButton = document.getElementById('refreshButton');
			refreshButton.addEventListener('click', function () {
				loadInventory();
			})
		};

		function addItemToDashboard(items) {
			if (dashboard) {
				dashboard.innerHTML = ''; 
				items.forEach(item => {
					const itemDiv = document.createElement('div');
					itemDiv.classList.add('item');
					if (item.categoria === 'headset') {
						itemDiv.innerHTML = `
						<div class="item-content">
							<h3>${item.categoria.toUpperCase()}</h3>
							<p>Local: ${item.localizacao}</p>
							<p>Entrada: ${item.headset_entrada}</p>
							<p>Microfone: ${item.headset_microfone}</p>
							<p>Quantidade: ${item.quantidade}</p>
							<p>Estado: ${item.estado}</p>
							<p>Marca: ${item.marca}</p>
						</div>
								`;
						dashboard.appendChild(itemDiv);
					} else if (item.categoria === 'equipamentos') {
						itemDiv.innerHTML = `
						<div class="item-content">
							<h3>${item.equip.toUpperCase()}</h3>
							<p>Local: ${item.localizacao}</p>
							<p>Quantidade: ${item.quantidade}</p>
							<p>Estado: ${item.estado}</p>
							<p>Marca: ${item.marca}</p>
							</div>
							`;
						dashboard.appendChild(itemDiv);
					} else if (item.categoria === 'monitor') {
						itemDiv.innerHTML = `
						<div class="item-content">
							<h3>${item.categoria.toUpperCase()}</h3>
							<p>Local: ${item.localizacao}</p>
							<p>Modelo: ${item.modelo}</p>
							<p>Quantidade: ${item.quantidade}</p>
							<p>Estado: ${item.estado}</p>
							<p>Marca: ${item.marca}</p>
							</div>
							`;
						dashboard.appendChild(itemDiv);
					} else {
						itemDiv.innerHTML = `
						<div class="item-content">
							<h3>${item.categoria.toUpperCase()}</h3>
							<p>Local: ${item.localizacao}</p>
							<p>Quantidade: ${item.quantidade}</p>
							<p>Estado: ${item.estado}</p>
							<p>Marca: ${item.marca}</p>
							</div>	
							`;
						dashboard.appendChild(itemDiv);
					};
				});
			};
		};

		if (dashboard) {
			function filterItems() {
				const filtroCategoria = document.getElementById('filtroCategoria').value.toLowerCase();
				const filtroEstado = document.getElementById('filtroEstado').value.toLowerCase();
				const filtroLocal = document.getElementById('filtroLocal').value.toLowerCase();

				fetch('./../../db/fetch.php')
					.then(response => response.json())
					.then(data => {
						let filteredItems = data.filter(item => {
							return (filtroCategoria === '' || item.categoria.toLowerCase().includes(filtroCategoria)) &&
								(filtroEstado === '' || item.estado.toLowerCase() === filtroEstado) && (filtroLocal === '' || item.localizacao.toLowerCase() === filtroLocal);
						});

						addItemToDashboard(filteredItems);
					})
					.catch(error => console.error('Erro ao filtrar itens:', error));
			};
		};

		if (filterForm) {
			filterForm.addEventListener('submit', function (event) {
				event.preventDefault();
				filterItems();
			});
		} else {
			console.error('Formulário de filtro não encontrado.');
		}
	
		function loadInventory() {
			fetch('./../../db/fetch.php')
				.then(response => response.json())
				.then(data => {
					if (dashboard) {
						dashboard.innerHTML = '';
						data.forEach(item => {
							filterItems(item);
						});
					};
				})
				.catch(error => console.error('Erro ao carregar inventário:', error));
		};
	});
} catch (e) {
	console.error(e);
};